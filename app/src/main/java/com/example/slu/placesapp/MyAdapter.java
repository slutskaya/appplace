package com.example.slu.placesapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by slu on 19.07.18.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Place> mDataSet;
    private MyLocationInterface callback;


    public MyAdapter(Context context, ArrayList<Place> myDataSet) {
        mDataSet = myDataSet;
        mContext = context;
        callback = (MyLocationInterface)mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_recycler_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.titleTextView.setText(mDataSet.get(position).getTitle());
        if (mDataSet.get(position).getIcon()==null){
            holder.iconImageView.setImageResource(R.mipmap.ic_launcher_round);
        }else {
            Picasso.with(mContext).load(mDataSet.get(position).getIcon()).into(holder.iconImageView);
        }

        if (mDataSet.get(position).getPlacePhoto()==null){
            holder.placePhotoImageView.setImageResource(R.mipmap.ic_launcher_round);
        }else {
            Picasso.with(mContext).load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" +
                    mDataSet.get(position).getPlacePhoto() + "&key=AIzaSyDr1b8tw9JJUFUSjAa4CHsUhC60E4ySqTI").into(holder.placePhotoImageView);
        }

        if(mDataSet.get(position).getOpen()){
            holder.isOpenTextView.setText("Open now");
        } else {
            holder.isOpenTextView.setText("Close now");
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onRecyclerViewItemClick(mDataSet.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTextView;
        public ImageView iconImageView;
        public TextView isOpenTextView;
        public ImageView placePhotoImageView;



        public ViewHolder(View itemView) {
            super(itemView);

            titleTextView = (TextView)itemView.findViewById(R.id.place_name_text_view);
            iconImageView = (ImageView)itemView.findViewById(R.id.place_icon_image_view);
            isOpenTextView = (TextView)itemView.findViewById(R.id.is_open_text_view);
            placePhotoImageView = (ImageView)itemView.findViewById(R.id.place_image_view);
        }
    }
}

