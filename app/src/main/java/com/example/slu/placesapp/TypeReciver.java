package com.example.slu.placesapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class TypeReciver extends BroadcastReceiver {


    private static final String TAG ="ADV_Service" ;
    private MyLocationInterface mCallback;


    public TypeReciver(MyLocationInterface callback) {
        mCallback = callback;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final ArrayList<Place> list = new ArrayList<>();
        Log.d(TAG, "MainActivity --> Received local broadcast!");
        String str="";

        if(intent.hasExtra("NearBy") || intent.hasExtra("ByText") ){
            str = intent.getStringExtra("JSON");

            try {

                JSONObject object = new JSONObject(str);
                JSONArray resultsArray = object.getJSONArray("results");

                ArrayList<JSONObject> resultsObjectsArray = new ArrayList<>();
                for (int i=0; i<resultsArray.length(); i++) {
                    resultsObjectsArray.add(resultsArray.getJSONObject(i));
                }

                ArrayList<JSONArray> arrayListOfPhotosArray = new ArrayList<>();
                for (int i=0; i<resultsArray.length(); i++) {
                    if (resultsObjectsArray.get(i).has("photos")) {
                        arrayListOfPhotosArray.add(resultsObjectsArray.get(i).getJSONArray("photos"));
                    } else {arrayListOfPhotosArray.add(null);}

                }

                ArrayList<String> photoReferenceSringArray = new ArrayList<>();
                for (int i=0; i<resultsArray.length(); i++) {
                    if (arrayListOfPhotosArray.get(i)!=null) {
                        photoReferenceSringArray.add(arrayListOfPhotosArray.get(i).getJSONObject(0).getString("photo_reference"));
                    } else {photoReferenceSringArray.add("");
                    }

                }

                ArrayList<JSONObject> objects = new ArrayList<>();
                ArrayList<JSONObject> geometryJSONObjects = new ArrayList<>();
                ArrayList<JSONObject> locationJSONObjects = new ArrayList<>();

                ArrayList<Double> latitudeArray = new ArrayList<>();
                ArrayList<Double> longtitudeArray = new ArrayList<>();
                ArrayList<String> namesOfPlasesArray = new ArrayList<>();

                for (int i=0; i<resultsArray.length(); i++) {
                    objects.add(resultsArray.getJSONObject(i));
                    geometryJSONObjects.add(objects.get(i).getJSONObject("geometry"));
                    locationJSONObjects.add(geometryJSONObjects.get(i).getJSONObject("location"));
                    latitudeArray.add(locationJSONObjects.get(i).getDouble("lat"));
                    longtitudeArray.add(locationJSONObjects.get(i).getDouble("lng"));
                    namesOfPlasesArray.add(objects.get(i).getString("name"));
                    list.add(new Place(namesOfPlasesArray.get(i),
                            locationJSONObjects.get(i).getDouble("lat"),
                            locationJSONObjects.get(i).getDouble("lng"),
                            objects.get(i).getString("icon"),
                            true,
                            photoReferenceSringArray.get(i)
                    ));
                }
                mCallback.onNearByClick(list);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }




    }






}