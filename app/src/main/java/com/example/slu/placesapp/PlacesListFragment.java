package com.example.slu.placesapp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by slu on 19.07.18.
 */

public class PlacesListFragment extends Fragment {

    private Context mContext;
    private MyLocationInterface callback;
    private FloatingActionButton nearByButton;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MyAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.places_list_fragment, container, false);
        mContext = getContext();

        nearByButton = v.findViewById(R.id.fab);
        nearByButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback = (MyLocationInterface)mContext;
                callback.onNearByButtonClick();

            }
        });
        return v;
    }

    void updateView(ArrayList<Place> placesList){
        mRecyclerView = (RecyclerView) ((MainActivity)mContext).findViewById(R.id.recycler_view);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager((MainActivity)mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setClickable(true);
        mRecyclerView.setFocusable(true);
        mRecyclerView.setFocusableInTouchMode(true);

        mAdapter = new MyAdapter((MainActivity)mContext, placesList);
        mRecyclerView.setAdapter(mAdapter);
    }
}
