package com.example.slu.placesapp;

import java.util.ArrayList;

public interface MyLocationInterface {
    void onNearByClick(ArrayList<Place> placesList);
    void onNearByButtonClick();
    void OnPlaceClick(String placeString);
    void onRecyclerViewItemClick(Place place);
}
