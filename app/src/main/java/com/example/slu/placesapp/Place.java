package com.example.slu.placesapp;

/**
 * Created by slu on 19.07.18.
 */

public class Place {

    private String title;
    private Double longtitude;
    private Double latitude;
    private String icon;
    private Boolean isOpen;
    private String placePhoto;

    public Place(String title, Double latitude, Double longtitude, String icon, Boolean isOpen, String placePhoto) {
        this.title = title;
        this.longtitude = longtitude;
        this.latitude = latitude;
        this.icon = icon;
        this.isOpen = isOpen;
        this.placePhoto = placePhoto;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(Double longtitude) {
        this.longtitude = longtitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public String getPlacePhoto() {
        return placePhoto;
    }

    public void setPlacePhoto(String placePhoto) {
        this.placePhoto = placePhoto;
    }
}
