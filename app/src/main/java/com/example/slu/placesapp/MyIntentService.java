package com.example.slu.placesapp;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyIntentService extends IntentService {
    public static final String BROADCAST_INDENTIFIER_FOR_SERVICE_FINISHED_RESPONSE =
            "com.example.slu.placesapp.MyIntentService";
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_NEAR_BY = "com.example.slu.placesapp.action.FOO";
    private static final String ACTION_BY_TEXT = "com.example.slu.placesapp.action.BAZ";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "com.example.slu.placesapp.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "com.example.slu.placesapp.extra.PARAM2";
    private static final String EXTRA_PARAM3 = "com.example.slu.placesapp.extra.PARAM3";

    public MyIntentService() {
        super("MyIntentService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionNearBy(Context context, String param1, String param2, String param3) {
        Intent intent = new Intent(context, MyIntentService.class);
        intent.setAction(ACTION_NEAR_BY);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        intent.putExtra(EXTRA_PARAM3, param3);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionByText(Context context, String param1, String param2, String param3) {
        Intent intent = new Intent(context, MyIntentService.class);
        intent.setAction(ACTION_BY_TEXT);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        intent.putExtra(EXTRA_PARAM3, param3);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_NEAR_BY.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                final String param3 = intent.getStringExtra(EXTRA_PARAM3);
                handleActionNearBy(param1, param2, param3);

            } else if (ACTION_BY_TEXT.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                final String param3 = intent.getStringExtra(EXTRA_PARAM3);
                handleActionByText(param1, param2, param3);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionNearBy(String param1, String param2, String param3) {
        String url;
        String jsn;
        Intent intent;

        url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + param1 + "," + param2 + "&radius=500&type=" + param3 + "&key=AIzaSyDr1b8tw9JJUFUSjAa4CHsUhC60E4ySqTI";

        jsn = sentHttpRequest(url);
        intent = new Intent(BROADCAST_INDENTIFIER_FOR_SERVICE_FINISHED_RESPONSE);

        intent.putExtra("NearBy", "NearBy");
        intent.putExtra("JSON", jsn);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionByText(String param1, String param2, String text) {
        // TODO: Handle action Baz
        String url;
        String jsn;
        Intent intent;

        url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query="+text+"&location=" + param1 + "," + param2 + "&radius=5000&key=AIzaSyDr1b8tw9JJUFUSjAa4CHsUhC60E4ySqTI";

        jsn = sentHttpRequest(url);
        intent = new Intent(BROADCAST_INDENTIFIER_FOR_SERVICE_FINISHED_RESPONSE);

        intent.putExtra("ByText", "ByText");
        intent.putExtra("JSON", jsn);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private String sentHttpRequest(String urlString) {
        BufferedReader input = null;
        HttpURLConnection httpCon = null;
        InputStream input_stream = null;
        InputStreamReader input_stream_reader = null;
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL(urlString);
            httpCon = (HttpURLConnection) url.openConnection();
            if (httpCon.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.e("TAG", "Cannot Connect to : " + urlString);
                return null;
            }

            input_stream = httpCon.getInputStream();
            input_stream_reader = new InputStreamReader(input_stream);
            input = new BufferedReader(input_stream_reader);
            String line;
            while ((line = input.readLine()) != null) {
                response.append(line + "\n");
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input_stream_reader.close();
                    input_stream.close();
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (httpCon != null) {
                    httpCon.disconnect();
                }
            }
        }
        return response.toString();
    }

}
