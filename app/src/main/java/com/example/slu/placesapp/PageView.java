package com.example.slu.placesapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PageView extends FragmentPagerAdapter {

    int numOfTabs;

    public PageView(FragmentManager fm, int i) {
        super(fm);
        this.numOfTabs = i;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                RecyclerFragment recyclerFragment = new RecyclerFragment();
                return recyclerFragment;


            case 1:
                MapFragment mapsFragment = new MapFragment();
                return mapsFragment;

//            case 2: NearbyFragment nearbyFragment = new NearbyFragment();
//                return  nearbyFragment;

            case 2: PlacesListFragment placesListFragment = new PlacesListFragment();
                return  placesListFragment;


        }

        return null;
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
