package com.example.slu.placesapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements
        MyLocationInterface{

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mToggle;
    private android.widget.SearchView searchView;
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final String TAG = "MainActivity";
    protected Location mLocation;
    private FusedLocationProviderClient mFused;
    private ViewPager viewPager;
    private TypeReciver receiver;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFused = LocationServices.getFusedLocationProviderClient(this);
        getDeviceLocation(this);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer);
        mToggle = new ActionBarDrawerToggle(this,mDrawerLayout,R.string.open,R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        searchView = (android.widget.SearchView) findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String text = "";
                try {
                    text = URLEncoder.encode(query,"UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                MyIntentService.startActionByText(MainActivity.this,mLocation.getLatitude()+"",mLocation.getLongitude()+"", text);
                IntentFilter filter = new IntentFilter(MyIntentService.BROADCAST_INDENTIFIER_FOR_SERVICE_FINISHED_RESPONSE);
                receiver = new TypeReciver(MainActivity.this) ;
                LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(receiver, filter);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(findViewById(R.id.smallDevice) != null){
            TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
            tabLayout.addTab(tabLayout.newTab().setText("Main"));
            tabLayout.addTab(tabLayout.newTab().setText("Maps"));
            tabLayout.addTab(tabLayout.newTab().setText("Buttons"));
            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
            viewPager = (ViewPager) findViewById(R.id.pager);
            final PageView pageView = new PageView
                    (getSupportFragmentManager(),tabLayout.getTabCount());

            viewPager.setAdapter(pageView);
            viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    viewPager.setCurrentItem(tab.getPosition());
                    if(tab.getPosition()==0){

                    }
                    if (tab.getPosition()==1){

                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });


        }
    }

    private void getDeviceLocation(final Context context){

        try{
            if((ContextCompat.checkSelfPermission(this, COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)&&
                    (ContextCompat.checkSelfPermission(this, FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)){
                @SuppressLint("MissingPermission") Task location =  mFused.getLastLocation();

                location.addOnCompleteListener(this,new OnCompleteListener() {

                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful()&&task.getResult()!=null){
                            mLocation = (Location) task.getResult();

                        }



                    }

                });

            }

        }catch (SecurityException e){
            Log.e(TAG, "getDeviceLocation: SecurityException" +e.getMessage());
        }
    }

    @Override
    public void onNearByClick(ArrayList<Place> placesList) {
        String tag2 = "android:switcher:" + R.id.pager + ":" + 2;
        String tag1 = "android:switcher:" + R.id.pager + ":" + 1;
        PlacesListFragment placesListFragment = (PlacesListFragment)getSupportFragmentManager().findFragmentByTag(tag2);
        placesListFragment.updateView(placesList);
        MapFragment mapFragment = (MapFragment)getSupportFragmentManager().findFragmentByTag(tag1);
        mapFragment.displayReceivedDataFromSearch(placesList);
    }

    @Override
    public void onNearByButtonClick() {
        FragmentManager fm = getSupportFragmentManager();
        NearByDialogFragment editNameDialogFragment = NearByDialogFragment.newInstance("Some Title");
        editNameDialogFragment.show(fm, "fragment_edit_name");
    }

    @Override
    public void OnPlaceClick(String placeString) {
        MyIntentService.startActionNearBy(MainActivity.this,mLocation.getLatitude()+"",mLocation.getLongitude()+"", placeString);
        IntentFilter filter = new IntentFilter(MyIntentService.BROADCAST_INDENTIFIER_FOR_SERVICE_FINISHED_RESPONSE);
        receiver = new TypeReciver(this) ;
        LocalBroadcastManager.getInstance(MainActivity.this).registerReceiver(receiver, filter);
    }

    @Override
    public void onRecyclerViewItemClick(Place place) {
        String tag = "android:switcher:" + R.id.pager + ":" + 1;
        MapFragment mapFragment = (MapFragment)getSupportFragmentManager().findFragmentByTag(tag);
        mapFragment.displayReceivedData(place);
        viewPager.setCurrentItem(1);
    }


}
