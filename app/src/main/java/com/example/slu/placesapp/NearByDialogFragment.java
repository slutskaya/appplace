package com.example.slu.placesapp;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by slu on 19.07.18.
 */

public class NearByDialogFragment extends DialogFragment {

    private Context mContext;
    private MyLocationInterface callback;
    private String airString = "airport";
    private String barString = "bar";
    private String gasString = "gas_station";
    private String gymString = "gym";
    private String hospitalString = "hospital";
    private String bankString = "bank";
    private String museumString = "museum";
    private String parkingString = "parking";
    private String resString  = "restaurant";
    private String schoolString = "school";
    private String shoppingString = "shopping_mall";
    private String zooString = "zoo";



    public NearByDialogFragment() {
    }

    public static NearByDialogFragment newInstance(String title) {
        NearByDialogFragment frag = new NearByDialogFragment();
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.near_by_fragment, container);

        ImageView imageView_airport = v.findViewById(R.id.imageView_airport);
        imageView_airport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callback = (MyLocationInterface)mContext;
                callback.OnPlaceClick(airString);
                dismiss();

            }
        });

        ImageView imageView_beer = v.findViewById(R.id.imageView_beer);
        imageView_beer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback = (MyLocationInterface)mContext;
                callback.OnPlaceClick(barString);
                dismiss();
            }
        });
        ImageView imageView_gas_station = v.findViewById(R.id.imageView_gas_station);
        imageView_gas_station.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback = (MyLocationInterface)mContext;
                callback.OnPlaceClick(gasString);
                dismiss();
            }
        });

        ImageView imageView_gym = v.findViewById(R.id.imageView_gym);
        imageView_gym.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback = (MyLocationInterface)mContext;
                callback.OnPlaceClick(gymString);
                dismiss();
            }
        });

        ImageView imageView_hospital = v.findViewById(R.id.imageView_hospital);
        imageView_hospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback = (MyLocationInterface)mContext;
                callback.OnPlaceClick(hospitalString);
                dismiss();
            }
        });

        ImageView imageView_money = v.findViewById(R.id.imageView_money);
        imageView_money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback = (MyLocationInterface)mContext;
                callback.OnPlaceClick(bankString);
                dismiss();
            }
        });

        ImageView imageView_museum = v.findViewById(R.id.imageView_museum);
        imageView_museum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback = (MyLocationInterface)mContext;
                callback.OnPlaceClick(museumString);
                dismiss();
            }
        });

        ImageView imageView_parking = v.findViewById(R.id.imageView_parking);
        imageView_parking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback = (MyLocationInterface)mContext;
                callback.OnPlaceClick(parkingString);
                dismiss();
            }
        });

        ImageView imageView_restaurant = v.findViewById(R.id.imageView_restaurant);
        imageView_restaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback = (MyLocationInterface)mContext;
                callback.OnPlaceClick(resString);
                dismiss();
            }
        });

        ImageView imageView_school = v.findViewById(R.id.imageView_school);
        imageView_school.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback = (MyLocationInterface)mContext;
                callback.OnPlaceClick(schoolString);
                dismiss();
            }
        });

        ImageView imageView_t_shirt = v.findViewById(R.id.imageView_t_shirt);
        imageView_t_shirt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback = (MyLocationInterface)mContext;
                callback.OnPlaceClick(shoppingString);
                dismiss();
            }
        });

        ImageView imageView_zoo = v.findViewById(R.id.imageView_zoo);
        imageView_zoo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback = (MyLocationInterface)mContext;
                callback.OnPlaceClick(zooString);
                dismiss();
            }
        });


        mContext = getContext();
        return v;
    }



}